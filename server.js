const express = require('express');
const multer = require('multer');
const crypto = require('crypto');
const fs = require('fs-extra');
const path = require('path');
const archiver = require('archiver');

const app = express();
const port = 3000;

const upload = multer({ dest: 'uploads/' });
const keys = new Map();

app.use(express.static('public'));
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

// Handle file uploads
app.post('/upload', upload.array('files'), (req, res) => {
    const key = crypto.randomBytes(100).toString('hex');
    const folderPath = path.join(__dirname, 'uploads', key);

    fs.mkdirSync(folderPath);
    req.files.forEach(file => {
        const dest = path.join(folderPath, file.originalname);
        fs.moveSync(file.path, dest);
    });

    keys.set(key, folderPath);
    res.json({ key });
});

// Handle file download
app.get('/download/:key', (req, res) => {
    const key = req.params.key;
    const folderPath = keys.get(key);

    if (folderPath) {
        const zipPath = `${folderPath}.zip`;
        const output = fs.createWriteStream(zipPath);
        const archive = archiver('zip');

        output.on('close', () => {
            res.download(zipPath, err => {
                if (err) {
                    console.error('Error in downloading file:', err);
                    res.status(500).send('Error in downloading file');
                } else {
                    fs.unlinkSync(zipPath);
                }
            });
        });

        archive.on('error', err => {
            throw err;
        });

        archive.pipe(output);
        archive.directory(folderPath, false);
        archive.finalize();
    } else {
        res.status(404).send('Invalid key');
    }
});

app.listen(port, () => {
    console.log(`Ultra app listening at http://localhost:${port}`);
});
